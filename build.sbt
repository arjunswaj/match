name := "Kundali"

version := "0.1"

scalaVersion := "2.11.11"
libraryDependencies += "org.typelevel" % "cats-core_2.11" % "0.9.0"
libraryDependencies += "org.typelevel" % "cats-free_2.11" % "0.9.0"
libraryDependencies += "org.apache.commons" % "commons-csv" % "1.4"
libraryDependencies += "net.htmlparser.jericho" % "jericho-html" % "3.4"
libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5.3"