package com.asb.kundali.eo

import java.time.{LocalDate, LocalTime}

object EOs {

  case class Place(city: String, state: String, country: String, lat: Double, lng: Double, tz: String)

  case class Person(name: String, date: LocalDate, time: LocalTime, place: Place, ayanamsha: String)

  case class Score(boy: Person, girl: Person, score: List[Double])

}