package com.asb.kundali

import java.time.{LocalDate, LocalTime, Month}

import cats.{Id, ~>}
import com.asb.kundali.dsl.CSVUtils.CSVIOs
import com.asb.kundali.dsl.FileUtils.FileIOs
import com.asb.kundali.dsl.KundaliUtils.KundaliActions
import com.asb.kundali.dsl.PersonUtils.PersonActions
import com.asb.kundali.dsl.ScoreUtils.ScoreActions
import com.asb.kundali.eo.EOs.{Person, Place}
import com.asb.kundali.interpreters.PersonInterpreters.YoungerPersonInterpreter
import com.asb.kundali.interpreters._
import com.asb.kundali.program.KundaliMatcher
import com.asb.kundali.program.KundaliMatcher.{P1, P2, P3, P4}

object KundaliMatcherCLI {

  implicit val F: FileIOs[P4] = FileIOs[P4]
  implicit val C: CSVIOs[P4] = CSVIOs[P4]
  implicit val P: PersonActions[P4] = PersonActions[P4]
  implicit val K: KundaliActions[P4] = KundaliActions[P4]
  implicit val S: ScoreActions[P4] = ScoreActions[P4]


  val i1: P1 ~> Id = FileInterpreter or CSVInterpreter
  val i2: P2 ~> Id = YoungerPersonInterpreter or i1
  val i3: P3 ~> Id = RealKundaliInterpreter.interpreter or i2
  val i4: P4 ~> Id = ScoreInterpreter or i3

  def main(args: Array[String]): Unit = {
    val place = Place("Bengaluru", "Karnataka", "India", 12.9717, 77.5936, "Asia/Kolkata")
    val p1 = Person("BB", LocalDate.of(1989, Month.SEPTEMBER, 11), LocalTime.of(12, 40), place, "chitrapaksha")
    val p2 = Person("GG", LocalDate.of(1989, Month.SEPTEMBER, 11), LocalTime.of(12, 40), place, "chitrapaksha")

    val timeFrame = (5 * 365) + 90

    KundaliMatcher
      .computeKundaliScores("result.csv", p1, p2, timeFrame, "en", K.findScoresOfGirlProspects)
      .foldMap(i4)
  }

}
