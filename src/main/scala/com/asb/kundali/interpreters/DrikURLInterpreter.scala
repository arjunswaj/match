package com.asb.kundali.interpreters

import java.time.format.DateTimeFormatter

import cats.{Id, ~>}
import com.asb.kundali.dsl.URLUtils.{BuildURL, URLAction}
import org.apache.http.client.utils.URIBuilder

object DrikURLInterpreter extends (URLAction ~> Id) {
  val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/YYYY")
  val timeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss")

  override def apply[A](fa: URLAction[A]): Id[A] = fa match {
    case BuildURL(boy, girl, lang) =>
      val builder = new URIBuilder()

      builder.setScheme("http")
      builder.setHost("www.drikpanchang.com")
      builder.setPath("/dkphp/dp-utilities/ajax/jyotisha/kundali-milan/dp-match.php")

      builder.addParameter("kmb-date", boy.date.format(dateTimeFormatter))
      builder.addParameter("kmb-time", boy.time.format(timeFormatter))
      builder.addParameter("kmb-name", boy.name)
      builder.addParameter("kmb-city", boy.place.city)
      builder.addParameter("kmb-state", boy.place.state)
      builder.addParameter("kmb-country", boy.place.country)
      builder.addParameter("kmb-lat", s"${boy.place.lat}")
      builder.addParameter("kmb-long", s"${boy.place.lng}")
      builder.addParameter("kmb-olsontz", boy.place.tz)
      builder.addParameter("kmb-ayanamsha", boy.ayanamsha)

      builder.addParameter("kmg-date", girl.date.format(dateTimeFormatter))
      builder.addParameter("kmg-time", girl.time.format(timeFormatter))
      builder.addParameter("kmg-name", girl.name)
      builder.addParameter("kmg-city", girl.place.city)
      builder.addParameter("kmg-state", girl.place.state)
      builder.addParameter("kmg-country", girl.place.country)
      builder.addParameter("kmg-lat", s"${girl.place.lat}")
      builder.addParameter("kmg-long", s"${girl.place.lng}")
      builder.addParameter("kmg-olsontz", girl.place.tz)
      builder.addParameter("kmg-ayanamsha", girl.ayanamsha)

      builder.addParameter("k-lang", lang)

      builder.build().toURL

  }
}
