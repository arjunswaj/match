package com.asb.kundali.interpreters

import java.nio.file.{Files, Paths}

import cats.{Id, ~>}
import com.asb.kundali.dsl.FileUtils._

object FileInterpreter extends (FileIO ~> Id) {
  override def apply[A](fa: FileIO[A]): Id[A] = fa match {
    case GetFilePath(filename) => Paths.get(filename)
    case GetBufferedReader(path) => Files.newBufferedReader(path)
    case Close(closable) => closable.close()
    case GetBufferedWriter(path) => Files.newBufferedWriter(path)
  }
}
