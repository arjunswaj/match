package com.asb.kundali.interpreters

import java.time.format.DateTimeFormatter

import cats.data.Coproduct
import cats.free.Free
import cats.{Id, ~>}
import com.asb.kundali.dsl.KundaliUtils.{FindScore, KundaliAction}
import com.asb.kundali.dsl.KundaliWebService.{KundaliWS, KundaliWSs}
import com.asb.kundali.dsl.LogUtils.{LogAction, LogActions}
import com.asb.kundali.dsl.URLUtils.{URLAction, URLActions}
import com.asb.kundali.eo.EOs.{Person, Score}

object RealKundaliInterpreter {

  type P1[A] = Coproduct[LogAction, URLAction, A]
  type P2[A] = Coproduct[KundaliWS, P1, A]
  type P2F[A] = Free[P2, A]

  implicit val L: LogActions[P2] = LogActions[P2]
  implicit val U: URLActions[P2] = URLActions[P2]
  implicit val K: KundaliWSs[P2] = KundaliWSs[P2]
  val interpreter: KundaliAction ~> Id = InternalInterpreter andThen ComponentsInterpreter

  def program(boy: Person, girl: Person, lang: String)
             (implicit L: LogActions[P2], U: URLActions[P2], K: KundaliWSs[P2]): P2F[Score] =
    for {
      url <- U.buildURL(boy, girl, lang)
      _ <- L.info(s"URL for boy: ${boy.date.format(DateTimeFormatter.ISO_DATE)} and " +
        s"girl ${girl.date.format(DateTimeFormatter.ISO_DATE)} is ${url.toString}")
      score <- K.getMatchScore(url)
    } yield Score(boy, girl, score)

  object InternalInterpreter extends (KundaliAction ~> P2F) {
    override def apply[A](fa: KundaliAction[A]): P2F[A] = fa match {
      case FindScore(boy, girl, lang) => program(boy, girl, lang)
    }
  }

  object ComponentsInterpreter extends (P2F ~> Id) {
    val i1: P1 ~> Id = LogInterpreter or DrikURLInterpreter
    val i2: P2 ~> Id = JerichoKundaliWSInterpreter or i1

    override def apply[A](fa: P2F[A]): Id[A] = fa.foldMap(i2)
  }
}
