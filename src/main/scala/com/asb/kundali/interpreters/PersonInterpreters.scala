package com.asb.kundali.interpreters

import cats.{Id, ~>}
import com.asb.kundali.dsl.PersonUtils.{BuildNthDayPerson, PersonAction}
import com.asb.kundali.eo.EOs.Person

object PersonInterpreters {

  object YoungerPersonInterpreter extends (PersonAction ~> Id) {
    override def apply[A](fa: PersonAction[A]): Id[A] = fa match {
      case BuildNthDayPerson(person, n) =>
        Person(person.name, person.date.plusDays(n), person.time, person.place, person.ayanamsha)
    }
  }

  object ElderPersonInterpreter extends (PersonAction ~> Id) {
    override def apply[A](fa: PersonAction[A]): Id[A] = fa match {
      case BuildNthDayPerson(person, n) =>
        Person(person.name, person.date.minusDays(n), person.time, person.place, person.ayanamsha)
    }
  }

}
