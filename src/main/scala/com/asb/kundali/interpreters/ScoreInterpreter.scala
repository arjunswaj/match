package com.asb.kundali.interpreters

import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE

import cats.{Id, ~>}
import com.asb.kundali.dsl.ScoreUtils.{ScoreAction, SerialiseScore}

object ScoreInterpreter extends (ScoreAction ~> Id) {

  import scala.collection.JavaConverters._


  override def apply[A](fa: ScoreAction[A]): Id[A] = fa match {
    case SerialiseScore(score) =>
      val boy = score.boy
      val girl = score.girl
      val points = score.score.map(_.toString)
      val sum = score.score.sum.toString
      val serialised: List[String] = boy.date.format(ISO_LOCAL_DATE) :: girl.date.format(ISO_LOCAL_DATE) :: sum :: points
      serialised.asJava
  }
}
