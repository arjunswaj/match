package com.asb.kundali.interpreters

import cats.{Id, ~>}
import com.asb.kundali.dsl.KundaliWebService.{GetMatchScore, KundaliWS}
import net.htmlparser.jericho.Source

import scala.util.Try

object JerichoKundaliWSInterpreter extends (KundaliWS ~> Id) {

  import scala.collection.JavaConverters._

  override def apply[A](fa: KundaliWS[A]): Id[A] = fa match {
    case GetMatchScore(url) =>
      try {
        val con = url.openConnection
        con.setConnectTimeout(60000)
        con.setReadTimeout(60000)
        val source = new Source(con)
        val table = Some(source.getAllElements("table class=\"dpJyotishTbl\""))
          .filter(elem => elem.size() > 1)
          .map(elem => elem.get(1))
          .map(elem => elem.getAllElements("tr"))
          .map(list => list.asScala
            .map(tr => tr.getChildElements)
            .filter(ch => ch.size() > 2)
            .map(ch => ch.get(2))
            .map(el => el.getTextExtractor.toString)
            .flatMap(el => Try(el.toDouble).toOption)
            .toList)
          .getOrElse(List[Double](-1, -1, -1, -1, -1, -1, -1, -1))
        println(s"Extracted from Drik: $table")

        table
      } catch {
        case ex: Exception =>
          println(s"Error: ${ex.getMessage}")
          List(-2, -2, -2, -2, -2, -2, -2, -2)
      }
  }
}
