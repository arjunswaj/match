package com.asb.kundali.interpreters

import cats.{Id, ~>}
import com.asb.kundali.dsl.LogUtils.{Error, Info, LogAction}

object LogInterpreter extends (LogAction ~> Id) {
  override def apply[A](fa: LogAction[A]): Id[A] = fa match {
    case Info(msg) => println(msg)
    case Error(msg, throwable) =>
      println(msg)
      println(throwable.getMessage)
  }
}
