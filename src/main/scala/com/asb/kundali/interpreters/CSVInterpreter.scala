package com.asb.kundali.interpreters

import java.io.Reader
import java.lang.Iterable

import cats.{Id, ~>}
import com.asb.kundali.dsl.CSVUtils._
import org.apache.commons.csv.{CSVFormat, CSVPrinter, CSVRecord}

object CSVInterpreter extends (CSVIO ~> Id) {
  override def apply[A](fa: CSVIO[A]): Id[A] = fa match {
    case GetCSVFormat() => CSVFormat.DEFAULT.withRecordSeparator("\n")
    case GetCSVPrinter(writer, format) => new CSVPrinter(writer, format)
    case ReadCSV(reader) => CSVOperations.readCSV(reader)
    case WriteCSV(printer, record) => CSVOperations.writeCSV(printer, record)
  }

  object CSVOperations {

    import scala.collection.JavaConverters._

    def readCSV(reader: Reader): Id[Stream[CSVRecord]] = {
      try {
        CSVFormat.RFC4180
          .withFirstRecordAsHeader()
          .parse(reader)
          .getRecords.iterator().asScala.toStream
      } catch {
        case ex: Exception => Stream.empty[CSVRecord]
      }
    }

    def writeCSV(printer: CSVPrinter, record: Iterable[String]): Id[Unit] = {
      try {
        printer.printRecord(record)
      } catch {
        case ex: Exception => ()
      }
    }

  }

}
