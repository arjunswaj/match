package com.asb.kundali.interpreters

import java.time.format.DateTimeFormatter

import cats.{Id, ~>}
import com.asb.kundali.dsl.KundaliUtils.{FindScore, KundaliAction}
import com.asb.kundali.eo.EOs.Score

object TestKundaliInterpreter extends (KundaliAction ~> Id) {
  override def apply[A](fa: KundaliAction[A]): Id[A] = fa match {
    case FindScore(boy, girl, lang) =>
      println(s"Finding score for ${girl.date.format(DateTimeFormatter.ISO_DATE)}")
      Score(boy, girl, List(1, 2, 3, 4, 5, 6, 7, 8))
  }
}
