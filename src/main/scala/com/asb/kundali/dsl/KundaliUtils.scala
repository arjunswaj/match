package com.asb.kundali.dsl

import cats.free.Free.inject
import cats.free.{Free, Inject}
import cats.implicits._
import com.asb.kundali.eo.EOs.{Person, Score}

object KundaliUtils {

  sealed trait KundaliAction[R]

  case class FindScore(boy: Person, girl: Person, lang: String) extends KundaliAction[Score]

  class KundaliActions[F[_]](implicit I: Inject[KundaliAction, F]) {

    type KundaliF[A] = Free[F, A]

    def findScoresOfGirlProspects(boy: Person, prospects: Stream[Person], lang: String): KundaliF[Stream[Score]] =
      prospects.map(girl => findScore(boy, girl, lang))
        .sequence[KundaliF, Score]

    def findScore(boy: Person, girl: Person, lang: String): KundaliF[Score] =
      inject(FindScore(boy, girl, lang))

    def findScoresOfBoyProspects(girl: Person, prospects: Stream[Person], lang: String): KundaliF[Stream[Score]] =
      prospects.map(boy => findScore(boy, girl, lang))
        .sequence[KundaliF, Score]

  }

  object KundaliActions {
    def apply[F[_]](implicit I: Inject[KundaliAction, F]): KundaliActions[F] = new KundaliActions[F]
  }

}
