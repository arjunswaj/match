package com.asb.kundali.dsl

import java.io.{Reader, Writer}
import java.lang.Iterable

import cats.free.Free.inject
import cats.free.{Free, Inject}
import cats.implicits._
import org.apache.commons.csv.{CSVFormat, CSVPrinter, CSVRecord}

object CSVUtils {

  sealed trait CSVIO[A]

  case class GetCSVFormat() extends CSVIO[CSVFormat]

  case class GetCSVPrinter(writer: Writer, format: CSVFormat) extends CSVIO[CSVPrinter]

  case class ReadCSV(reader: Reader) extends CSVIO[Stream[CSVRecord]]

  case class WriteCSV(printer: CSVPrinter, record: Iterable[String]) extends CSVIO[Unit]

  class CSVIOs[F[_]](implicit I: Inject[CSVIO, F]) {

    type CSVF[A] = Free[F, A]

    def getCSVFormat: CSVF[CSVFormat] =
      inject(GetCSVFormat())

    def getCSVPrinter(writer: Writer, format: CSVFormat): CSVF[CSVPrinter] =
      inject(GetCSVPrinter(writer, format))

    def readCSV(reader: Reader): CSVF[Stream[CSVRecord]] =
      inject(ReadCSV(reader))

    def writeRecordsToCSV(printer: CSVPrinter, records: Stream[Iterable[String]]): CSVF[Stream[Unit]] =
      records.map(record => writeRecordToCSV(printer, record))
        .sequence[CSVF, Unit]

    def writeRecordToCSV(printer: CSVPrinter, record: Iterable[String]): CSVF[Unit] =
      inject(WriteCSV(printer, record))

  }

  object CSVIOs {
    implicit def apply[F[_]](implicit I: Inject[CSVIO, F]): CSVIOs[F] = new CSVIOs[F]
  }

}
