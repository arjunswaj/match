package com.asb.kundali.dsl

import java.net.URL

import cats.free.Free.inject
import cats.free.{Free, Inject}

object KundaliWebService {

  sealed trait KundaliWS[A]

  case class GetMatchScore(url: URL) extends KundaliWS[List[Double]]

  class KundaliWSs[F[_]](implicit I: Inject[KundaliWS, F]) {
    type KundaliWSF[A] = Free[F, A]

    def getMatchScore(url: URL): KundaliWSF[List[Double]] =
      inject(GetMatchScore(url))
  }

  object KundaliWSs {
    def apply[F[_]](implicit I: Inject[KundaliWS, F]): KundaliWSs[F] = new KundaliWSs[F]
  }

}
