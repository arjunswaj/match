package com.asb.kundali.dsl

import java.net.URL

import cats.free.Free.inject
import cats.free.{Free, Inject}
import com.asb.kundali.eo.EOs.Person

object URLUtils {

  sealed trait URLAction[A]

  case class BuildURL(boy: Person, girl: Person, lang: String) extends URLAction[URL]

  class URLActions[F[_]](implicit I: Inject[URLAction, F]) {
    type URLF[A] = Free[F, A]

    def buildURL(boy: Person, girl: Person, lang: String): URLF[URL] =
      inject(BuildURL(boy, girl, lang))
  }

  object URLActions {
    def apply[F[_]](implicit I: Inject[URLAction, F]): URLActions[F] = new URLActions[F]
  }

}
