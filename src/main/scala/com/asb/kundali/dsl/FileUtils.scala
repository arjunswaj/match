package com.asb.kundali.dsl

import java.io._
import java.nio.file.Path

import cats.free.Free.inject
import cats.free.{Free, Inject}

object FileUtils {

  sealed trait FileIO[A]

  case class GetFilePath(filename: String) extends FileIO[Path]

  case class GetBufferedReader(path: Path) extends FileIO[BufferedReader]

  case class GetBufferedWriter(path: Path) extends FileIO[BufferedWriter]

  case class Close(closable: Closeable) extends FileIO[Unit]

  class FileIOs[F[_]](implicit I: Inject[FileIO, F]) {

    def getFilePath(string: String): Free[F, Path] =
      inject(GetFilePath(string))

    def getBufferedReader(path: Path): Free[F, BufferedReader] =
      inject(GetBufferedReader(path))

    def getBufferedWriter(path: Path): Free[F, BufferedWriter] =
      inject(GetBufferedWriter(path))

    def close(closable: Closeable): Free[F, Unit] =
      inject(Close(closable))

  }

  object FileIOs {
    def apply[F[_]](implicit I: Inject[FileIO, F]): FileIOs[F] = new FileIOs[F]
  }

}
