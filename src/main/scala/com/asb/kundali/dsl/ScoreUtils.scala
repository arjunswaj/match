package com.asb.kundali.dsl

import java.lang.Iterable

import cats.free.Free.inject
import cats.free.{Free, Inject}
import cats.implicits._
import com.asb.kundali.eo.EOs.Score

object ScoreUtils {

  sealed trait ScoreAction[A]

  case class SerialiseScore(score: Score) extends ScoreAction[Iterable[String]]

  class ScoreActions[F[_]](implicit I: Inject[ScoreAction, F]) {

    type ScoreF[A] = Free[F, A]

    def serialiseScores(scores: Stream[Score]): ScoreF[Stream[Iterable[String]]] =
      scores.map(score => serialiseScore(score))
        .sequence[ScoreF, Iterable[String]]

    def serialiseScore(score: Score): ScoreF[Iterable[String]] =
      inject(SerialiseScore(score))

  }

  object ScoreActions {
    def apply[F[_]](implicit I: Inject[ScoreAction, F]): ScoreActions[F] = new ScoreActions[F]
  }

}
