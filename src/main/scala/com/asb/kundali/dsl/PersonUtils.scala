package com.asb.kundali.dsl

import cats.free.Free.inject
import cats.free.{Free, Inject}
import cats.implicits._
import com.asb.kundali.eo.EOs.Person

object PersonUtils {

  sealed trait PersonAction[A]

  case class BuildNthDayPerson(person: Person, n: Int) extends PersonAction[Person]

  class PersonActions[F[_]](implicit I: Inject[PersonAction, F]) {

    type PersonsF[A] = Free[F, A]

    def buildPersonsForNDays(person: Person, n: Int): PersonsF[Stream[Person]] =
      Stream.range(0, n)
        .map(i => buildNthDayPerson(person, i))
        .sequence[PersonsF, Person]

    def buildNthDayPerson(person: Person, n: Int): PersonsF[Person] =
      inject(BuildNthDayPerson(person, n))

  }

  object PersonActions {
    def apply[F[_]](implicit I: Inject[PersonAction, F]): PersonActions[F] = new PersonActions[F]
  }

}
