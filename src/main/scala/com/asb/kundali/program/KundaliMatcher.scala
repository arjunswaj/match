package com.asb.kundali.program

import java.nio.file.Path

import cats.data.Coproduct
import cats.free.Free
import com.asb.kundali.dsl.CSVUtils.{CSVIO, CSVIOs}
import com.asb.kundali.dsl.FileUtils.{FileIO, FileIOs}
import com.asb.kundali.dsl.KundaliUtils.{KundaliAction, KundaliActions}
import com.asb.kundali.dsl.PersonUtils.{PersonAction, PersonActions}
import com.asb.kundali.dsl.ScoreUtils.{ScoreAction, ScoreActions}
import com.asb.kundali.eo.EOs.{Person, Score}

object KundaliMatcher {

  type P1[A] = Coproduct[FileIO, CSVIO, A]
  type P2[A] = Coproduct[PersonAction, P1, A]
  type P3[A] = Coproduct[KundaliAction, P2, A]
  type P4[A] = Coproduct[ScoreAction, P3, A]

  def computeKundaliScores(resultFile: String, p1: Person, p2: Person, n: Int, lang: String,
                           scoreFinder: (Person, Stream[Person], String) => Free[P4, Stream[Score]])
                          (implicit F: FileIOs[P4], C: CSVIOs[P4], P: PersonActions[P4],
                           K: KundaliActions[P4], S: ScoreActions[P4]): Free[P4, Path] =
    for {
      path <- F.getFilePath(resultFile)
      writer <- F.getBufferedWriter(path)
      csvFormat <- C.getCSVFormat
      printer <- C.getCSVPrinter(writer, csvFormat)
      prospects <- P.buildPersonsForNDays(p2, n)
      scores <- scoreFinder(p1, prospects, lang)
      serialised <- S.serialiseScores(scores)
      _ <- C.writeRecordsToCSV(printer, serialised)
      _ <- F.close(printer)
      _ <- F.close(writer)
    } yield path


}
