###Optimal Kundali calculator
This is a script written in scala using Free Monads to get the score using birth dates of two individuals. This does not calculate the score but makes a call to DrikPanchang to get the score.

### License
This project is released under the WTFPL License.

*Disclaimer: I do not endorse this in anyway, just wanted to use Free Monads to write a functional script.*